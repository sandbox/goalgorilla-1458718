<?php

/**
 * @file
 * Ogpay paypal callbacks
 *
 * This file handles paypal callbacks via POST
 */
 
/**
 * Menu callback
 */
function ogpay_paypal_callback($nid) {
	global $user;

	$group_node = node_load($nid);

  module_load_include('inc', 'ogpay', 'ogpay.functions');

	$get_params = array(
		'TOKEN' => $_GET['token'],
	);	

	$result = ogpay_paypal_api_post('GetExpressCheckoutDetails', $get_params);	

	$do_params = array(
		'TOKEN' => $result['TOKEN'],
		'AMT' => floatval($group_node->ogpay_price),
		'PAYERID' => $result['PAYERID'],
		'DESC' => ogpay_get_description($group_node),		
		'CURRENCYCODE' => variable_get('ogpay_currency', ''),
		'PROFILESTARTDATE' => date('c'),
		'BILLINGPERIOD' => variable_get('ogpay_billing_period', ''),
		'BILLINGFREQUENCY' => variable_get('ogpay_billing_frequency', ''),
		'TOTALBILLINGCYCLES' => NULL,
		'NOTIFYURL' => $base_url . '/paypal/ipn',		
		'SHIPPINGAMT' => NULL,
		'TAXAMT' => NULL,		
		'SUBSCRIBERNAME' => $result['SHIPTONAME'],
		'PROFILEREFERENCE' => 1,
		'MAXFAILEDPAYMENTS' => 1,
		'AUTOBILLAMT' => 'AddToNextBilling',
	);	

	$result = ogpay_paypal_api_post('CreateRecurringPaymentsProfile', $do_params);	

	if (isset($result['PROFILEID']) && isset($result['ACK']) && $result['ACK'] == 'Success') {
	
		$profile_id = $result['PROFILEID'];
		
		$existing_record = db_result(db_query("SELECT * FROM {ogpay_profiles} WHERE profile='%s'", $profile_id));					
		
		$record = array(
			'profile' => $profile_id,		
			'uid' => $user->uid,
			'nid' => arg(1),
		);					
		
		// Update or create db record				
		if ($existing_record) {
			drupal_write_record('ogpay_profiles', $record, 'pid'); 					
		}
		else {
			drupal_write_record('ogpay_profiles', $record); 					
		}						
	
		og_save_subscription($nid, $user->uid, array('is_active' => TRUE));
	
		drupal_set_message(t('All done'));
		
		// Redirect to the journal page
		drupal_goto('node/' . arg(1));
	}
	else {
		if (isset($result['PROFILEID'])) {
			drupal_set_message(t('Error, please send an email to the site administrators with the following token: !token', array('!token' => $result['PROFILEID'])));
		}
		else {
			drupal_set_message(t('Error, please send an email to the site administrators'));			
		}
	  if (module_exists('devel')) {
			dpm($result);
		}
	}
	
	return '';	
}
