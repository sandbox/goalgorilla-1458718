<?php

/**
 * @file
 * Ogpay settings
 *
 * This file handles settings
 */

/**
 * Menu callback, settings form
 */
function ogpay_settings_form($form_state) {
	$form = array();
	
	$form['ogpay_username'] = array(
		'#type' => 'textfield',
		'#title' => t('API Username'),
		'#default_value' => variable_get('ogpay_username', ''),
	);	
		
	$form['ogpay_password'] = array(
		'#type' => 'textfield',
		'#title' => t('API Password'),
		'#default_value' => variable_get('ogpay_password', ''),
	);	

	$form['ogpay_signature'] = array(
		'#type' => 'textfield',
		'#title' => t('API Signature'),
		'#default_value' => variable_get('ogpay_signature', ''),
	);			
		
	$endpoints = array(
		'paypal.com' => t('Live'),
		'sandbox.paypal.com' => t('Sandbox'),
		'beta-sandbox.paypal.com' => t('Beta sandbox'),		
	);		
		
	$form['ogpay_endpoint'] = array(
		'#type' => 'select',
		'#title' => t('End point'),
		'#options' => $endpoints,
		'#default_value' => variable_get('ogpay_endpoint', ''),
	);			
	
	$currencies = array(
		'GBP' => t('Ponds'), 
		'EUR' => t("Euro's"), 
		'JPY' => t('Yen'),
		'USD' => t('Dollars'),
	);	
	
	$form['ogpay_currency'] = array(
		'#type' => 'select',
		'#title' => t('Currency'),
		'#options' => $currencies,
		'#default_value' => variable_get('ogpay_currency', ''),
	);		
		
	$billing_periods = array(
    'Day' => t('Day'), 
		'Week' => t('Week'),
	  'SemiMonth'=> t('Semi month'),
		'Month'=> t('Month'),
		'Year' => t('Year'),
	);	
	
	$form['ogpay_billing_period'] = array(
		'#type' => 'select',
		'#title' => t('Billing period'),
		'#options' => $billing_periods,
		'#default_value' => variable_get('ogpay_billing_period', ''),
	);		
				
	$form['ogpay_billing_frequency'] = array(
		'#type' => 'textfield',
		'#title' => t('Billing frequency'),
		'#default_value' => variable_get('ogpay_billing_frequency', ''),
		'#description' => t('Enter a number'),
	);						
				
	$og_groups = og_get_types('group');
	$all_node_types = node_get_types();
	
	foreach ($og_groups as $og_group) {
		$og_group_options[$og_group] = $all_node_types[$og_group]->name;
	}		
		
	$form['ogpay_payable_groups'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Select the groups that are payable'),
		'#options' => $og_group_options,
		'#default_value' => variable_get('ogpay_payable_groups', array()),	
	);		
			
	return system_settings_form($form);	
}