CONTENTS
--------
 
* Description
* Requirements
* Installation
* How to use


DESCRIPTION
-----------------
 
The ogpay module adds the possibility to have organic groups as a payed service.
Users can buy group access via paypal. 
This will be a subscription, with site-wide configured periods.


REQUIREMENTS
-----------------

* Organic groups
* Paypal sandbox, get your own at: http://developer.paypal.com


INSTALLATION
-----------------

* Enable the module like every other module.
* Set permissions at http://example.com/admin/user/permissions
  (Anonymous users can never buy a subscription)
* Go to http://example.com/admin/og/ogpay.
* Get your API key, API signature and API password from the paypal sandbox.
* Select sandbox as endpoint, set currency, billing period and billing frequency
* As last, set the payable group types
* Very important, enter your ipn callback in paypal.
	The IPN callback is http://example.com/paypal/ipn


HOW TO USE
-----------------

Now you can select payable when creating a organic group.
You have to enter a price.
Users can buy a subscription when they have the right permissions.