<?php

/**
 * Handle an incoming IPN
 *
 * PayPal sends an IPN here for each transaction that takes place. The IPN
 * is present as a form submission which this routine unravels and saves for
 * processing.
 */
function ogpay_paypal_ipn_in() {
	watchdog('Paypal IPN', 'Incoming');				
	$request = "cmd=_notify-validate"; 
	foreach ($_POST as $varname => $varvalue){
		$email .= "$varname: $varvalue\n";  
		if(function_exists('get_magic_quotes_gpc') and get_magic_quotes_gpc()){  
			$varvalue = urlencode(stripslashes($varvalue)); 
		}
		else { 
			$value = urlencode($value); 
		} 
		$request .= "&$varname=$varvalue"; 
	} 
	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL,"https://www.sandbox.paypal.com/cgi-bin/webscr");
	//curl_setopt($ch,CURLOPT_URL,"https://www.paypal.com");

	curl_setopt($ch,CURLOPT_POST,true);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$request);
	curl_setopt($ch,CURLOPT_FOLLOWLOCATION,false);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	switch($result){
		case "VERIFIED":
				// verified payment
				watchdog('Paypal IPN', 'verified');
				watchdog('Paypal IPN', $request);
				
				ogpay_react_on_ipn($_POST);
			break;
		case "INVALID":
			// invalid/fake payment
				watchdog('Paypal IPN', 'invalid');
				watchdog('Paypal IPN', $request);		
			break;
		default:
			// any other case (such as no response, connection timeout...)
	}
}


/**
 * Processing of the paypal IPN.
 */
function ogpay_react_on_ipn($params) {
	if (isset($params['txn_type'])) {	
		switch($params['txn_type']) {
			case 'recurring_payment_profile_cancel':
				if(isset($params['recurring_payment_id'])) {
					// Cancel the subscription
					$q = db_query("SELECT uid, nid FROM {ogpay_profiles} WHERE profile='%s'", $params['recurring_payment_id']);
					while ($r = db_fetch_array($q)) {
					  $profile = $r;
					}				
					og_delete_subscription($profile['nid'], $profile['uid']);				
				}
				else {
					watchdog('Paypal IPN', 'Recurring payment cancel but no profile ID');
				}
			break;
			case 'recurring_payment_profile_created':
				if (isset($params['recurring_payment_id'])) {
					$q = db_query("SELECT nid, uid FROM {ogpay_profiles} WHERE profile='%s'", $params['recurring_payment_id']);
					while ($r = db_fetch_array($q)) {
					  $data = $r;
					}			
					og_save_subscription($data['nid'], $data['uid'], array('is_active' => TRUE));
				}
				else {
					watchdog('Paypal IPN', 'Recurring payment created but no profile ID');
				}								
			break;
		}
	}
}

/**
 * IPN logs
 */
function ogpay_paypal_log() {
	$type = 'Paypal IPN';	

  $result = pager_query("SELECT COUNT(wid) AS count, message, variables FROM {watchdog} WHERE type = '%s' GROUP BY message, variables ", 30, 0, "SELECT COUNT(DISTINCT(message)) FROM {watchdog} WHERE type = '%s'", $type);
	$rows = array();
  while ($dblog = db_fetch_object($result)) {
		if ($dblog->message != '[]') {
			$decoded = json_decode($dblog->message);  	
  		$rows[$decoded->payment_date] = $decoded;
  	}
  }
  if (module_exists('devel')) {
		dpm($rows);
	}
	else {
		drupal_set_message(t('Please enable the devel module'));	
	}
	return '';
}