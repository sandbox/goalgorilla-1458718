Drupal.behaviors.ogpayForm = function() {

	$('#edit-og-selective-0-wrapper,' +
		'#edit-og-selective-1-wrapper,' +
		'#edit-og-selective-2-wrapper,' +
		'#edit-og-selective-3-wrapper,' +
		'#edit-og-selective-4-wrapper').click(function() {
		checkSelected();
	});	
	
	function checkSelected () {
		if ($('#edit-og-selective-4:checked').length > 0) {
			$('#edit-ogpay-price-wrapper').fadeIn();
		}
		else {
			$('#edit-ogpay-price-wrapper').fadeOut();
		}		
	}
	
	checkSelected();
}