<?php

/**
 * Send HTTP POST Request
 *
 * @param $function	string The API method name
 * @param	$params array	keyed values
 * @return	array	Parsed HTTP Response body
 */
function ogpay_paypal_api_post($function, $params) {
	
	// Setting up API credentials, PayPal end point, and API version.
	$standard_params = array(
		'VERSION' => '51.0',
		'USER' => variable_get('ogpay_username', ''),
		'PWD' => variable_get('ogpay_password', ''),
		'SIGNATURE' => variable_get('ogpay_signature', ''),
	);	

	$endpoint = 'https://api-3t.' . variable_get('ogpay_endpoint', '') . '/nvp';

	// setting the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	// Set the curl parameters.
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	// Merge our parameters with the specified array
	$params = array_merge($standard_params, $params);

	// Dump it in a url
	foreach ($params as $key => $value) {
		$post_url .= '&' . $key . '=' . urlencode($value); 	
	}

	// Add the method to the post string
	$post_url = 'METHOD=' . $function . $post_url;

	// Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_url);

	// Get response from the server.
	$httpResponse = curl_exec($ch);
	
	
	if(!$httpResponse) {
		drupal_set_message(t('!function failed: !error on !num', array('!function' => $function, '!error' => curl_error($ch), '!num' => curl_errno($ch))), 'error');
	}

	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = urldecode($tmpAr[1]);
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}

	return $httpParsedResponseAr;
}

/**
 * Paypal trigger
 */
function ogpay_subscribe_to_group($nid) {
  // Load functions
	global $base_url;
  
	$group_node = node_load($nid);
  
	$set_params = array(
		'PAYMENTACTION' => 'Authorization',
		'CANCELURL' => $base_url . '/paypal/' . $nid . '/cancel',
		'RETURNURL' => $base_url . '/paypal/' . $nid . '/confirmation',
		'NOTIFYURL' => $base_url . '/paypal/ipn',
		'AMT' => floatval($group_node->ogpay_price),
		'BILLINGAGREEMENTDESCRIPTION' => ogpay_get_description($group_node),		
		'BILLINGTYPE' => 'RecurringPayments',
		'CURRENCYCODE' => variable_get('ogpay_currency', ''),
	);

	$set_return = ogpay_paypal_api_post('SetExpressCheckout', $set_params);	

	$token = $set_return['TOKEN'];
	
	$redirect_path = 'https://www.' . variable_get('ogpay_endpoint', '') . '/cgi‑bin/webscr&cmd=_express-checkout&token=' . urlencode($token);

	drupal_goto($redirect_path);
}

/**
 * Description for paypal
 */
function ogpay_get_description($group_node) {
	return t('You will be buying a group supscription for the group !groupname at !sitename, for the price of !price !currency', array(
		'!groupname' => $group_node->title,		
		'!price' => $group_node->ogpay_price,	
		'!currency' => variable_get('ogpay_currency', ''),
		'!sitename' => variable_get('site_name', ''),
	));
}
